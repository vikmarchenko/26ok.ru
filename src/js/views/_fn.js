$.fn.extend({
	$action: function(value) {
		return this.find('[data-action="' + value + '"]');
	},
	$view: function(value){
		return this.find('[data-view="' + value + '"]');
	}
});
window.ok26 = window.ok26 || {};
ok26.transitionend = transitionEnd(document.body).whichTransitionEnd();
ok26.scrollWidth = function(e){
	return window.innerWidth - $(window).width();
};
ok26.modalReplacer = function(e){
	e.preventDefault();
	var url = $(this).attr('data-href');
	$.get(url, function(data) {
		var $modal = $(data);
		$modal.$view('list-controls').css('right', ok26.scrollWidth() + 'px');
		$modal.on('show.bs.modal',function(e) {
			$('[data-view=header]').css('padding-right', ok26.scrollWidth() + 'px');
		});
		$modal.on('shown.bs.modal', function(e){
			ok26.Modal();
		});
		$modal.on('hidden.bs.modal', function(e){
			$('[data-view=header]').css('padding-right',$('body').css('padding-right'));
			$modal.remove();
		});
		$modal.modal();
	});
};
ok26.starClick = function(e){
	e.preventDefault();
	$(this).toggleClass('is-active');
};
ok26.loadMore = function(e){
	e.preventDefault();
	var $btn = $(this);
	var url = $btn.attr('data-url');
	var acceptor = $btn.attr('data-acceptor');
	$btn.attr('disabled', 'disabled');
	$btn.addClass('pending');
	setTimeout(function(){
		$.get(url, function(data){
			$('body').$view(acceptor).append(data);
			$btn.removeAttr('disabled', 'disabled');
			$btn.removeClass('pending');
		});
	}, 500);
};