window.ok26 = window.ok26 || {};
ok26.Modal = function(el_path){
	el_path = el_path || '[data-js="modal"]';
	ok26.Gallery(el_path + ' [data-js="gallery"]');
	ok26.Tabs(el_path + ' [data-js="tabs"]');
	ok26.Map(el_path + ' [data-view="map-shower"]');
	$(el_path + ' [data-action="toggle-star"]').on('click.starClick', ok26.starClick);
};