window.ok26 = window.ok26 || {};
window.ok26.mapCustom = {
	Pin: function(coords, hint){
		return new ymaps.Placemark(coords, {
		           hintContent: hint || 'Метка'
		       }, {
		           iconLayout: 'default#image',
		           iconImageHref: 'img/m_pin.png',
		           iconImageSize: [27, 34],
		           iconImageOffset: [-13, -34]
		});
	},
	Cluster: function(){
		return new ymaps.Clusterer({
            clusterIcons: [{
                href: 'img/m_cluster.png',
                size: [48, 48],
                offset: [-24, -24]
            }],
            clusterNumbers: [100]
        });
	}
};