window.ok26 = window.ok26 || {};
ok26.Fonts = function(){
	WebFontConfig = {
		google: { families: [ 'Roboto:300,400,700:latin,cyrillic' ] }
	};
	var wf = document.createElement('script');
	wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
		'://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
	wf.type = 'text/javascript';
	wf.async = 'true';
	var s = document.getElementsByTagName('script')[0];
	s.parentNode.insertBefore(wf, s);
};