window.ok26 = window.ok26 || {};
ok26.App = function(){
	ok26.Gallery();
	ok26.Tabs();
	ok26.Home();
	ok26.Search();
	ok26.Map();
	ok26.Agency();
	$('[data-view="bs-select"]').addClass('btn-group-sm').selectpicker({
		width: '100%',
		style: 'btn-link btn-group-sm'
	}).parent().removeClass('styled-select');
};
