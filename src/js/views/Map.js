window.ok26 = window.ok26 || {};
ok26.Map = function(el_path){
	el_path = el_path || '[data-view="map-shower"]';
	$(el_path).each(function(){
		var $map = $(this);
		if (!$map.data('map_applied')){
			ymaps.ready(function(){
				var mapObject = new ymaps.Map($map[0], {
				            center: $map.attr('data-center').split(',').map(parseFloat),
				            zoom: 14
				        });
				var placemark = new ymaps.Placemark($map.attr('data-placemark').split(',').map(parseFloat));
				mapObject.geoObjects.add(placemark);
			});
			$map.data('map_applied', true);
		}
	});
};