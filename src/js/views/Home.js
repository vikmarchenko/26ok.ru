window.ok26 = window.ok26 || {};
ok26.Home = function(){
	var $home = $('[data-js=home]');
	var $map = $home.$view('map');
	var mapObject = {
		fitToViewport: function(){}
	};
	var mapCreation = function(){
		mapObject = new ymaps.Map($map[0], {
		            center: $map.attr('data-center').split(',').map(parseFloat),
		            zoom: 12
		        });
		// Example placemark and cluster
		var placemark = ok26.mapCustom.Pin($map.attr('data-center').split(',').map(parseFloat));
		var cluster = ok26.mapCustom.Cluster();
		var points = [
		    [55.831903,37.411961], [55.763338,37.565466], [55.763338,37.565466], [55.744522,37.616378], [55.780898,37.642889], [55.793559,37.435983], [55.800584,37.675638], [55.716733,37.589988], [55.775724,37.560840], [55.822144,37.433781], [55.874170,37.669838], [55.716770,37.482338], [55.780850,37.750210], [55.810906,37.654142], [55.865386,37.713329], [55.847121,37.525797], [55.778655,37.710743], [55.623415,37.717934], [55.863193,37.737000], [55.866770,37.760113], [55.698261,37.730838], [55.633800,37.564769], [55.639996,37.539400], [55.690230,37.405853], [55.775970,37.512900], [55.775777,37.442180], [55.811814,37.440448], [55.751841,37.404853], [55.627303,37.728976], [55.816515,37.597163], [55.664352,37.689397], [55.679195,37.600961], [55.673873,37.658425], [55.681006,37.605126], [55.876327,37.431744], [55.843363,37.778445], [55.875445,37.549348], [55.662903,37.702087], [55.746099,37.434113], [55.838660,37.712326], [55.774838,37.415725], [55.871539,37.630223], [55.657037,37.571271], [55.691046,37.711026], [55.803972,37.659610], [55.616448,37.452759], [55.781329,37.442781], [55.844708,37.748870], [55.723123,37.406067], [55.858585,37.484980]
		];
		var geoObjects = [];
		for(var i = 0, len = points.length; i < len; i++) {
		    geoObjects[i] = ok26.mapCustom.Pin(points[i], 'Pin' + i);
		}
		cluster.add(geoObjects);
		mapObject.geoObjects.add(cluster);
		mapObject.container.fitToViewport();
	};
	// Showing popups on click (replace default listeners and apply listeners to block)
	$home.$view('aside-items').on('click.openModal', '[data-action="toggle-modal"]', ok26.modalReplacer);
	$home.$view('home-default').on('click.openModal', '[data-action="toggle-modal"]', ok26.modalReplacer);
	$home.$view('search-results').on('click.openModal', '[data-action="toggle-modal"]', ok26.modalReplacer);
	$home.$view('aside-items').on('click.starClick', '[data-action="toggle-star"]', ok26.starClick);
	$home.$view('home-default').on('click.starClick', '[data-action="toggle-star"]', ok26.starClick);
	$home.$view('search-results').on('click.starClick', '[data-action="toggle-star"]', ok26.starClick);
	// Showing map
	$home.on('loadMap', function(e, callback){
		$home.$view('map-aside').addClass('animate');
		if (!$home.$view('home-map').hasClass('active')){
			var width = $home.width() - 40;
			$home.$view('wrp').animate({width: width}, 400, 'linear', function(){
				// Body elements animations
				$home.$view('wrp').addClass('wide')
				                      .removeAttr('style');
				$home.$view('home-default').removeClass('active');
				$home.$view('home-search').removeClass('active');
				$home.$view('home-map').addClass('active');
				$home.$view('footer').addClass('fixed');
				if (!$map.data('mapAttached')){
					// Map attach
					$map.height($home.height() - $home.$view('header').outerHeight() - 5);
					ymaps.ready(mapCreation);
					$map.data('mapAttached', true);
					// Configure aside list
					$home.$view('aside-items').baron({
						bar: '.scroller__bar',
						track: '.scroller__track'
					});
					var timeout = 0;
					$(window).on('resize', function(e){
						clearTimeout(timeout);
						timeout = setTimeout(function(){
							$('body').$view('map').height($('body').height() - $('body').$view('header').outerHeight() -5);
							mapObject.container.fitToViewport();
						}, 200);
					});
				}
				callback();
			});
		} else {
			callback();
		}
	});
	// Hiding map
	$home.on('simpleSearch', function(e, callback){
		$home.$view('map-aside').removeClass('animate');
		if ($home.$view('map-aside').hasClass('active')){
			$home.$view('map-aside').removeClass('active');
			$map.css('margin-right', '');
			mapObject.container.fitToViewport();
		}
		if (!$home.$view('home-search').hasClass('active')){
			if ($home.$view('wrp').hasClass('wide')){
				var width = $('<div class="wrp"/>').appendTo('body').width();
				$home.$view('wrp').removeClass('wide')
				                  .width($home.width() - 40);
				$home.$view('wrp').animate({width: width}, 400, 'linear', function(){
					$home.$view('wrp').removeAttr('style');
					$home.$view('home-search').addClass('active');
					$home.$view('home-map').removeClass('active');
					$home.$view('home-default').removeClass('active');
					$home.$view('footer').removeClass('fixed');
					callback();
				});
			} else {
				$home.$view('home-search').addClass('active');
				$home.$view('home-map').removeClass('active');
				$home.$view('home-default').removeClass('active');
				callback();
			}
		} else {
			callback();
		}
	});
	// Toggle side panel over map
	$home.$action('toggle-aside').on('click', function(){
		if ($home.$view('map-aside').hasClass('active')){
			$map.css('margin-right', '');
			$home.$view('map-aside').removeClass('active');
			mapObject.container.fitToViewport();
		} else {
			$home.$view('map-aside').addClass('active');
		}
	});
	$home.$view('map-aside').on(ok26.transitionend, function(e){
		if ($home.$view('map-aside').hasClass('active')){
			$map.css('margin-right', $home.$view('map-aside').width() + 'px');
			mapObject.container.fitToViewport();
		}
	});
};