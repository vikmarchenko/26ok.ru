window.ok26 = window.ok26 || {};
ok26.Tabs = function(el_path){
	el_path = el_path || '[data-js=tabs]';
	$(el_path).each(function(){
		var $tabs = $(this);
		if (!$tabs.data('tabs_applied')){
			$tabs.data('tabs_applied', true);
			$tabs.addClass('js-tabs');
			$tabs.$action('switch-tab').on('click', function(e){
				e.preventDefault();
				var href = this.getAttribute('href');
				$tabs.$action('switch-tab').removeClass('active');
				$(this).addClass('active');
				$(href).addClass('active')
				    .siblings().removeClass('active');
			});
		}
	});
};