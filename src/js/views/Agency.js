window.ok26 = window.ok26 || {};
ok26.Agency = function(){
	var $agency = $('[data-js=agency]');
	$agency.$view('ads-list').on('click.toggleModal', '[data-action="toggle-modal"]', ok26.modalReplacer);
	$agency.$view('ads-list').on('click.toggleModal', '[data-action="toggle-star"]', ok26.starClick);
	$agency.$action('load-more').on('click.loadMore', ok26.loadMore);
	$agency.$action('offices-shower').on('click', function(){
		$agency.$view('offices-list').addClass('active');
		$(this).hide();
	});
	$agency.$action('show-full-article').on('click.showFull', function(e){
		e.preventDefault();
		$(this).hide();
		$($(this).attr('href')).addClass('visible');
	});
};