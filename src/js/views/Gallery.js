window.ok26 = window.ok26 || {};
ok26.Gallery = function(el_path){
	el_path = el_path || '[data-js="gallery"]';
	var actions = {
		// Check beginning and end of big slides
		checkSlidesPos: function(){
			this.slides_pos = (this.slides_pos < 0) ? (this.total - 1) : this.slides_pos;
			this.slides_pos = (this.slides_pos === this.total) ? 0 : this.slides_pos;
		},
		// Check beginning and end of small carousel
		checkThumsPos: function(withSlide){
			this.thumbs_pos = (this.thumbs_pos < 0) ? (this.total - 1) : this.thumbs_pos;
			this.thumbs_pos = (this.thumbs_pos >= this.total) ? 0 : this.thumbs_pos;
			if (withSlide){
				if (((this.slides_pos - this.thumbs_pos) > 4)||(this.thumbs_pos > this.slides_pos)){
					this.thumbs_pos = this.slides_pos;
				}
			}
			if (this.thumbs_pos > (this.total - 6)){
				this.thumbs_pos = this.total - 5;
			}
		},
		// Move big slides
		render: function(){
			actions.checkSlidesPos.apply(this);
			actions.checkThumsPos.apply(this, [true]);
			this.$gallery.$view('slides-list').css('x', - this.slides_pos * 5 + '%');
			this.$gallery.$view('thumbs-list').children()
			    .eq(this.slides_pos).addClass('active')
			    .siblings().removeClass('active');
			this.$gallery.$view('current-slide').text(this.slides_pos + 1);
			actions.renderThumbs.apply(this);
		},
		// Move small carousel
		renderThumbs: function(){
			this.$gallery.$view('thumbs-list').css('x', -this.thumbs_pos * 1 + '%');
		}
	};
	$(el_path).each(function(){
		var view = {
			$gallery   : $(this),
			slides_pos : 0,
			thumbs_pos : 0
		};
		if (!view.$gallery.data('gallery_applied')){
		view.$gallery.data('gallery_applied', true);
		view.total = view.$gallery.$view('slides-list').children().length;
		view.$gallery.$view('total-slide').text(view.total);
		actions.render.apply(view);
		// Big slides buttons
		view.$gallery.$action('prev-slide').on('click', function(){
			view.slides_pos--;
			actions.render.apply(view);
		});
		view.$gallery.$action('next-slide').on('click', function(){
			view.slides_pos++;
			actions.render.apply(view);
		});
		// Small carousel buttons
		view.$gallery.$action('prev-thumb').on('click', function(){
			view.thumbs_pos-=5;
			actions.checkThumsPos.apply(view, [false]);
			actions.renderThumbs.apply(view);
		});
		view.$gallery.$action('next-thumb').on('click', function(){
			view.thumbs_pos+=5;
			actions.checkThumsPos.apply(view, [false]);
			actions.renderThumbs.apply(view);
		});
		// Click on small slide
		view.$gallery.$action('select-slide').on('click', function(e){
			e.preventDefault();
			view.slides_pos = parseInt(this.getAttribute('data-index'), 10);
			actions.render.apply(view);
		});
		}
	});
};