window.ok26 = window.ok26 || {};
ok26.Search = function(){
	var el_path = el_path || '[data-js=search]';
	$(el_path).each(function(){
		var $search = $(this);
		var $home = $('body');
		$search.data('state', 'simple');
		// Show additional parameters
		$search.$action('toggle-subform').on('click', function(){
			$search.$action('toggle-subform').toggleClass('active');
			$search.$view('subform').toggleClass('active');
		});
		// Realty type select
		var $typeList = $search.$view('type-list'),
			$typeToggle = $typeList.$action('toggle-sublist'),
			$typeSublist = $typeList.$view('type-sublist');
		$typeList.find('input').on('change', function(){
			$typeToggle.toggleClass('active', this.getAttribute('data-type') === 'subitem');
			$typeSublist.removeClass('active');
			$search.$view('type-check__' + this.value).addClass('active').siblings().removeClass('active');
		});
		$typeToggle.on('click', function(){
			$typeSublist.toggleClass('active');
		});
		// Show map
		$search.$action('show-map').on('click', function(e){
			e.preventDefault();
			$(this).attr('disabled', 'disabled');
			$search.data('state', 'map');
			$home.trigger('loadMap', [function(){
				$search.$action('show-map').removeAttr('disabled');
			}]);
			return false;
		});
		// Simple search
		$search.$action('search').on('click', function(e){
			e.preventDefault();
			$(this).attr('disabled', 'disabled');
			$search.data('state', 'simple');
			$home.trigger('simpleSearch', [function(){
				$search.$action('search').removeAttr('disabled');
			}]);
			return false;
		});
		// Scroller
		$search.$view('search-labels').baron({
			bar: '.scroller__bar',
			track: '.scroller__track'
		});
	});
};