var express = require('express'),
    app = express();
var http = require('http').Server(app);

app.set('port', (process.env.PORT || 5000));
app.use(express.static(__dirname + '/app'));

http.listen(app.get('port'), function() {
	console.log('start listening');
});