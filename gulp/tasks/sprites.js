var gulp         = require('gulp');
var handleErrors = require('../util/handleErrors');
var config       = require('../config').sprites;
var sprity       = require('sprity');
var gulpif       = require('gulp-if');

gulp.task('sprites_raw', function () {
	return sprity.src({
		src: config.src,
		style: './icons.less',
		out: config.imgDest,
		engine: 'gm',
		cssPath: config.cssPath
	}).pipe(gulpif('*.png', gulp.dest(config.imgDest), gulp.dest(config.cssDest)))
});

gulp.task('sprites', ['sprites_raw', 'images', 'less']);