var gulp         = require('gulp');
var concat       = require('gulp-concat');
var jshint       = require('gulp-jshint');
var browserSync  = require('browser-sync');
var sourcemaps   = require('gulp-sourcemaps');
var uglify       = require('gulp-uglify');
var handleErrors = require('../util/handleErrors');
var rename       = require('gulp-rename');
var config       = require('../config').js;
var production   = require('../config').prod;
var gulpif       = require('gulp-if');

gulp.task('js_plugins', function(){
	return gulp.src(config['plugins'])
	    .pipe(concat('plugins' + '.js'))
	    .on('error', handleErrors)
	    .pipe(gulp.dest(config.dest))
	    .pipe(browserSync.reload({stream:true}));
});
gulp.task('js_plugins_min', function(){
	return gulp.src(config['plugins'])
	    .pipe(concat('plugins.min' + '.js'))
	    .on('error', handleErrors)
	    .pipe(uglify())
	    .on('error', handleErrors)
	    .pipe(gulp.dest(config.dest))
	    .pipe(browserSync.reload({stream:true}));
});
gulp.task('js_main', function(){
	return gulp.src(config['main'])
	    .pipe(sourcemaps.init())
	    .pipe(jshint())
	    .pipe(jshint.reporter('default'))
	    .pipe(concat('main' + '.js'))
	    .on('error', handleErrors)
	    .pipe(gulpif(production, uglify()))
	    .on('error', handleErrors)
	    .pipe(sourcemaps.write('./'))
	    .pipe(gulp.dest(config.dest))
	    .pipe(browserSync.reload({stream:true}));
});
gulp.task('js_main_build', function(){
	return gulp.src(config['main'])
	    .pipe(concat('main.min' + '.js'))
	    .pipe(uglify())
	    .pipe(gulp.dest(config.dest))
	    .pipe(browserSync.reload({stream:true}));
});
gulp.task('javascripts', ['js_plugins', 'js_main', 'js_main_build']);