/* Notes:
   - gulp/tasks/browserify.js handles js recompiling with watchify
   - gulp/tasks/browserSync.js watches and reloads compiled files
*/

var gulp   = require('gulp');
var config = require('../config');

gulp.task('watch', ['browserSync'], function(callback) {
  gulp.watch(config.less.watch, ['less']);
  gulp.watch(config.images.src, ['images']);
  gulp.watch(config.sprites.src, ['sprites']);
  gulp.watch(config.js.main, ['js_main']);
  gulp.watch(config.root.src + '/jade/**', ['jade']);
});
