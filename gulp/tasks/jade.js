var gulp = require('gulp');
var jade = require('gulp-jade');
var config = require('../config').jade;
var browserSync  = require('browser-sync');
var handleErrors = require('../util/handleErrors');


gulp.task('jade', function(cb){
	return gulp.src(config.src)
	    .pipe(jade({
	    	pretty: true
	    }))
	    .on('error', handleErrors)
	    .pipe(gulp.dest(config.dest))
	    .pipe(browserSync.reload({stream:true}));
});
