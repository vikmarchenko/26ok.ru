var gulp         = require('gulp');
var browserSync  = require('browser-sync');
var less         = require('gulp-less');
var cssmin       = require('gulp-cssmin');
var sourcemaps   = require('gulp-sourcemaps');
var handleErrors = require('../util/handleErrors');
var config       = require('../config').less;
var autoprefixer = require('gulp-autoprefixer');
var production   = require('../config').prod;
var gulpif       = require('gulp-if');
var rename       = require('gulp-rename');

gulp.task('less', function () {
  return gulp.src(config.src)
    .pipe(sourcemaps.init())
    .pipe(less(config.settings))
    .on('error', handleErrors)
    .pipe(autoprefixer({ browsers: ['last 2 version'] }))
    .pipe(gulpif(production, cssmin()))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(config.dest))
    .pipe(browserSync.stream({match:'**/*.css'}));
});

gulp.task('less_build', function () {
  return gulp.src(config.src)
    .pipe(less(config.settings))
    .pipe(autoprefixer({ browsers: ['last 2 version'] }))
    .pipe(cssmin())
    .pipe(rename('app.min.css'))
    .pipe(gulp.dest(config.dest))
});