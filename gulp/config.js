var appPath = "./app";
var src  = "./assets";
var bowerRoot = "./bower_components";

var config = {};
var root = {
	src: './src',
	dest: './app',
	bower: './bower_components'
};
config.root = root;
config.prod = false;
config.browserSync = {
	server: {
		baseDir: root.dest
	},
	files: [root.dest + '**']
};
config.jade = {
	src: root.src + '/jade/pages/*.jade',
	dest: root.dest
};
config.less = {
	watch: root.src + '/less/**',
	src: root.src + '/less/app.less',
	dest: root.dest + '/css'
};
config.js = {
	main: [
		root.src + '/js/views/*.js',
		root.src + '/js/start.js'
	],
	plugins: [
		root.bower + '/jquery/dist/jquery.js',
		root.bower + '/jquery.transit/jquery.transit.js',
		root.bower + '/fastclick/lib/fastclick.js',
		root.bower + '/baron/baron.js',
		root.bower + '/bootstrap/js/transition.js',
		root.bower + '/bootstrap/js/modal.js',
		root.bower + '/bootstrap/js/dropdown.js',
		root.bower + '/bootstrap-select/js/bootstrap-select.js',
		root.bower + '/bootstrap-select/js/i18n/defaults-ru_RU.js',
		root.bower + '/transitionEnd/src/transition-end.js'
	],
	dest: root.dest + '/js'
};
config.images = {
	src: root.src + '/img/**',
	dest: root.dest + '/img'
};
config.sprites = {
	src: root.src + '/icons/*.png',
	imgDest: root.src + '/img',
	cssDest: root.src + '/less',
	cssPath: '../img'
};

module.exports = config;