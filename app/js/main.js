window.ok26 = window.ok26 || {};
ok26.Agency = function(){
	var $agency = $('[data-js=agency]');
	$agency.$view('ads-list').on('click.toggleModal', '[data-action="toggle-modal"]', ok26.modalReplacer);
	$agency.$view('ads-list').on('click.toggleModal', '[data-action="toggle-star"]', ok26.starClick);
	$agency.$action('load-more').on('click.loadMore', ok26.loadMore);
	$agency.$action('offices-shower').on('click', function(){
		$agency.$view('offices-list').addClass('active');
		$(this).hide();
	});
	$agency.$action('show-full-article').on('click.showFull', function(e){
		e.preventDefault();
		$(this).hide();
		$($(this).attr('href')).addClass('visible');
	});
};
window.ok26 = window.ok26 || {};
ok26.App = function(){
	ok26.Gallery();
	ok26.Tabs();
	ok26.Home();
	ok26.Search();
	ok26.Map();
	ok26.Agency();
	$('[data-view="bs-select"]').addClass('btn-group-sm').selectpicker({
		width: '100%',
		style: 'btn-link btn-group-sm'
	}).parent().removeClass('styled-select');
};

window.ok26 = window.ok26 || {};
ok26.Fonts = function(){
	WebFontConfig = {
		google: { families: [ 'Roboto:300,400,700:latin,cyrillic' ] }
	};
	var wf = document.createElement('script');
	wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
		'://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
	wf.type = 'text/javascript';
	wf.async = 'true';
	var s = document.getElementsByTagName('script')[0];
	s.parentNode.insertBefore(wf, s);
};
window.ok26 = window.ok26 || {};
ok26.Gallery = function(el_path){
	el_path = el_path || '[data-js="gallery"]';
	var actions = {
		// Check beginning and end of big slides
		checkSlidesPos: function(){
			this.slides_pos = (this.slides_pos < 0) ? (this.total - 1) : this.slides_pos;
			this.slides_pos = (this.slides_pos === this.total) ? 0 : this.slides_pos;
		},
		// Check beginning and end of small carousel
		checkThumsPos: function(withSlide){
			this.thumbs_pos = (this.thumbs_pos < 0) ? (this.total - 1) : this.thumbs_pos;
			this.thumbs_pos = (this.thumbs_pos >= this.total) ? 0 : this.thumbs_pos;
			if (withSlide){
				if (((this.slides_pos - this.thumbs_pos) > 4)||(this.thumbs_pos > this.slides_pos)){
					this.thumbs_pos = this.slides_pos;
				}
			}
			if (this.thumbs_pos > (this.total - 6)){
				this.thumbs_pos = this.total - 5;
			}
		},
		// Move big slides
		render: function(){
			actions.checkSlidesPos.apply(this);
			actions.checkThumsPos.apply(this, [true]);
			this.$gallery.$view('slides-list').css('x', - this.slides_pos * 5 + '%');
			this.$gallery.$view('thumbs-list').children()
			    .eq(this.slides_pos).addClass('active')
			    .siblings().removeClass('active');
			this.$gallery.$view('current-slide').text(this.slides_pos + 1);
			actions.renderThumbs.apply(this);
		},
		// Move small carousel
		renderThumbs: function(){
			this.$gallery.$view('thumbs-list').css('x', -this.thumbs_pos * 1 + '%');
		}
	};
	$(el_path).each(function(){
		var view = {
			$gallery   : $(this),
			slides_pos : 0,
			thumbs_pos : 0
		};
		if (!view.$gallery.data('gallery_applied')){
		view.$gallery.data('gallery_applied', true);
		view.total = view.$gallery.$view('slides-list').children().length;
		view.$gallery.$view('total-slide').text(view.total);
		actions.render.apply(view);
		// Big slides buttons
		view.$gallery.$action('prev-slide').on('click', function(){
			view.slides_pos--;
			actions.render.apply(view);
		});
		view.$gallery.$action('next-slide').on('click', function(){
			view.slides_pos++;
			actions.render.apply(view);
		});
		// Small carousel buttons
		view.$gallery.$action('prev-thumb').on('click', function(){
			view.thumbs_pos-=5;
			actions.checkThumsPos.apply(view, [false]);
			actions.renderThumbs.apply(view);
		});
		view.$gallery.$action('next-thumb').on('click', function(){
			view.thumbs_pos+=5;
			actions.checkThumsPos.apply(view, [false]);
			actions.renderThumbs.apply(view);
		});
		// Click on small slide
		view.$gallery.$action('select-slide').on('click', function(e){
			e.preventDefault();
			view.slides_pos = parseInt(this.getAttribute('data-index'), 10);
			actions.render.apply(view);
		});
		}
	});
};
window.ok26 = window.ok26 || {};
ok26.Home = function(){
	var $home = $('[data-js=home]');
	var $map = $home.$view('map');
	var mapObject = {
		fitToViewport: function(){}
	};
	var mapCreation = function(){
		mapObject = new ymaps.Map($map[0], {
		            center: $map.attr('data-center').split(',').map(parseFloat),
		            zoom: 12
		        });
		// Example placemark and cluster
		var placemark = ok26.mapCustom.Pin($map.attr('data-center').split(',').map(parseFloat));
		var cluster = ok26.mapCustom.Cluster();
		var points = [
		    [55.831903,37.411961], [55.763338,37.565466], [55.763338,37.565466], [55.744522,37.616378], [55.780898,37.642889], [55.793559,37.435983], [55.800584,37.675638], [55.716733,37.589988], [55.775724,37.560840], [55.822144,37.433781], [55.874170,37.669838], [55.716770,37.482338], [55.780850,37.750210], [55.810906,37.654142], [55.865386,37.713329], [55.847121,37.525797], [55.778655,37.710743], [55.623415,37.717934], [55.863193,37.737000], [55.866770,37.760113], [55.698261,37.730838], [55.633800,37.564769], [55.639996,37.539400], [55.690230,37.405853], [55.775970,37.512900], [55.775777,37.442180], [55.811814,37.440448], [55.751841,37.404853], [55.627303,37.728976], [55.816515,37.597163], [55.664352,37.689397], [55.679195,37.600961], [55.673873,37.658425], [55.681006,37.605126], [55.876327,37.431744], [55.843363,37.778445], [55.875445,37.549348], [55.662903,37.702087], [55.746099,37.434113], [55.838660,37.712326], [55.774838,37.415725], [55.871539,37.630223], [55.657037,37.571271], [55.691046,37.711026], [55.803972,37.659610], [55.616448,37.452759], [55.781329,37.442781], [55.844708,37.748870], [55.723123,37.406067], [55.858585,37.484980]
		];
		var geoObjects = [];
		for(var i = 0, len = points.length; i < len; i++) {
		    geoObjects[i] = ok26.mapCustom.Pin(points[i], 'Pin' + i);
		}
		cluster.add(geoObjects);
		mapObject.geoObjects.add(cluster);
		mapObject.container.fitToViewport();
	};
	// Showing popups on click (replace default listeners and apply listeners to block)
	$home.$view('aside-items').on('click.openModal', '[data-action="toggle-modal"]', ok26.modalReplacer);
	$home.$view('home-default').on('click.openModal', '[data-action="toggle-modal"]', ok26.modalReplacer);
	$home.$view('search-results').on('click.openModal', '[data-action="toggle-modal"]', ok26.modalReplacer);
	$home.$view('aside-items').on('click.starClick', '[data-action="toggle-star"]', ok26.starClick);
	$home.$view('home-default').on('click.starClick', '[data-action="toggle-star"]', ok26.starClick);
	$home.$view('search-results').on('click.starClick', '[data-action="toggle-star"]', ok26.starClick);
	// Showing map
	$home.on('loadMap', function(e, callback){
		$home.$view('map-aside').addClass('animate');
		if (!$home.$view('home-map').hasClass('active')){
			var width = $home.width() - 40;
			$home.$view('wrp').animate({width: width}, 400, 'linear', function(){
				// Body elements animations
				$home.$view('wrp').addClass('wide')
				                      .removeAttr('style');
				$home.$view('home-default').removeClass('active');
				$home.$view('home-search').removeClass('active');
				$home.$view('home-map').addClass('active');
				$home.$view('footer').addClass('fixed');
				if (!$map.data('mapAttached')){
					// Map attach
					$map.height($home.height() - $home.$view('header').outerHeight() - 5);
					ymaps.ready(mapCreation);
					$map.data('mapAttached', true);
					// Configure aside list
					$home.$view('aside-items').baron({
						bar: '.scroller__bar',
						track: '.scroller__track'
					});
					var timeout = 0;
					$(window).on('resize', function(e){
						clearTimeout(timeout);
						timeout = setTimeout(function(){
							$('body').$view('map').height($('body').height() - $('body').$view('header').outerHeight() -5);
							mapObject.container.fitToViewport();
						}, 200);
					});
				}
				callback();
			});
		} else {
			callback();
		}
	});
	// Hiding map
	$home.on('simpleSearch', function(e, callback){
		$home.$view('map-aside').removeClass('animate');
		if ($home.$view('map-aside').hasClass('active')){
			$home.$view('map-aside').removeClass('active');
			$map.css('margin-right', '');
			mapObject.container.fitToViewport();
		}
		if (!$home.$view('home-search').hasClass('active')){
			if ($home.$view('wrp').hasClass('wide')){
				var width = $('<div class="wrp"/>').appendTo('body').width();
				$home.$view('wrp').removeClass('wide')
				                  .width($home.width() - 40);
				$home.$view('wrp').animate({width: width}, 400, 'linear', function(){
					$home.$view('wrp').removeAttr('style');
					$home.$view('home-search').addClass('active');
					$home.$view('home-map').removeClass('active');
					$home.$view('home-default').removeClass('active');
					$home.$view('footer').removeClass('fixed');
					callback();
				});
			} else {
				$home.$view('home-search').addClass('active');
				$home.$view('home-map').removeClass('active');
				$home.$view('home-default').removeClass('active');
				callback();
			}
		} else {
			callback();
		}
	});
	// Toggle side panel over map
	$home.$action('toggle-aside').on('click', function(){
		if ($home.$view('map-aside').hasClass('active')){
			$map.css('margin-right', '');
			$home.$view('map-aside').removeClass('active');
			mapObject.container.fitToViewport();
		} else {
			$home.$view('map-aside').addClass('active');
		}
	});
	$home.$view('map-aside').on(ok26.transitionend, function(e){
		if ($home.$view('map-aside').hasClass('active')){
			$map.css('margin-right', $home.$view('map-aside').width() + 'px');
			mapObject.container.fitToViewport();
		}
	});
};
window.ok26 = window.ok26 || {};
ok26.Map = function(el_path){
	el_path = el_path || '[data-view="map-shower"]';
	$(el_path).each(function(){
		var $map = $(this);
		if (!$map.data('map_applied')){
			ymaps.ready(function(){
				var mapObject = new ymaps.Map($map[0], {
				            center: $map.attr('data-center').split(',').map(parseFloat),
				            zoom: 14
				        });
				var placemark = new ymaps.Placemark($map.attr('data-placemark').split(',').map(parseFloat));
				mapObject.geoObjects.add(placemark);
			});
			$map.data('map_applied', true);
		}
	});
};
window.ok26 = window.ok26 || {};
ok26.Modal = function(el_path){
	el_path = el_path || '[data-js="modal"]';
	ok26.Gallery(el_path + ' [data-js="gallery"]');
	ok26.Tabs(el_path + ' [data-js="tabs"]');
	ok26.Map(el_path + ' [data-view="map-shower"]');
	$(el_path + ' [data-action="toggle-star"]').on('click.starClick', ok26.starClick);
};
window.ok26 = window.ok26 || {};
ok26.Search = function(){
	var el_path = el_path || '[data-js=search]';
	$(el_path).each(function(){
		var $search = $(this);
		var $home = $('body');
		$search.data('state', 'simple');
		// Show additional parameters
		$search.$action('toggle-subform').on('click', function(){
			$search.$action('toggle-subform').toggleClass('active');
			$search.$view('subform').toggleClass('active');
		});
		// Realty type select
		var $typeList = $search.$view('type-list'),
			$typeToggle = $typeList.$action('toggle-sublist'),
			$typeSublist = $typeList.$view('type-sublist');
		$typeList.find('input').on('change', function(){
			$typeToggle.toggleClass('active', this.getAttribute('data-type') === 'subitem');
			$typeSublist.removeClass('active');
			$search.$view('type-check__' + this.value).addClass('active').siblings().removeClass('active');
		});
		$typeToggle.on('click', function(){
			$typeSublist.toggleClass('active');
		});
		// Show map
		$search.$action('show-map').on('click', function(e){
			e.preventDefault();
			$(this).attr('disabled', 'disabled');
			$search.data('state', 'map');
			$home.trigger('loadMap', [function(){
				$search.$action('show-map').removeAttr('disabled');
			}]);
			return false;
		});
		// Simple search
		$search.$action('search').on('click', function(e){
			e.preventDefault();
			$(this).attr('disabled', 'disabled');
			$search.data('state', 'simple');
			$home.trigger('simpleSearch', [function(){
				$search.$action('search').removeAttr('disabled');
			}]);
			return false;
		});
		// Scroller
		$search.$view('search-labels').baron({
			bar: '.scroller__bar',
			track: '.scroller__track'
		});
	});
};
window.ok26 = window.ok26 || {};
ok26.Tabs = function(el_path){
	el_path = el_path || '[data-js=tabs]';
	$(el_path).each(function(){
		var $tabs = $(this);
		if (!$tabs.data('tabs_applied')){
			$tabs.data('tabs_applied', true);
			$tabs.addClass('js-tabs');
			$tabs.$action('switch-tab').on('click', function(e){
				e.preventDefault();
				var href = this.getAttribute('href');
				$tabs.$action('switch-tab').removeClass('active');
				$(this).addClass('active');
				$(href).addClass('active')
				    .siblings().removeClass('active');
			});
		}
	});
};
$.fn.extend({
	$action: function(value) {
		return this.find('[data-action="' + value + '"]');
	},
	$view: function(value){
		return this.find('[data-view="' + value + '"]');
	}
});
window.ok26 = window.ok26 || {};
ok26.transitionend = transitionEnd(document.body).whichTransitionEnd();
ok26.scrollWidth = function(e){
	return window.innerWidth - $(window).width();
};
ok26.modalReplacer = function(e){
	e.preventDefault();
	var url = $(this).attr('data-href');
	$.get(url, function(data) {
		var $modal = $(data);
		$modal.$view('list-controls').css('right', ok26.scrollWidth() + 'px');
		$modal.on('show.bs.modal',function(e) {
			$('[data-view=header]').css('padding-right', ok26.scrollWidth() + 'px');
		});
		$modal.on('shown.bs.modal', function(e){
			ok26.Modal();
		});
		$modal.on('hidden.bs.modal', function(e){
			$('[data-view=header]').css('padding-right',$('body').css('padding-right'));
			$modal.remove();
		});
		$modal.modal();
	});
};
ok26.starClick = function(e){
	e.preventDefault();
	$(this).toggleClass('is-active');
};
ok26.loadMore = function(e){
	e.preventDefault();
	var $btn = $(this);
	var url = $btn.attr('data-url');
	var acceptor = $btn.attr('data-acceptor');
	$btn.attr('disabled', 'disabled');
	$btn.addClass('pending');
	setTimeout(function(){
		$.get(url, function(data){
			$('body').$view(acceptor).append(data);
			$btn.removeAttr('disabled', 'disabled');
			$btn.removeClass('pending');
		});
	}, 500);
};
window.ok26 = window.ok26 || {};
window.ok26.mapCustom = {
	Pin: function(coords, hint){
		return new ymaps.Placemark(coords, {
		           hintContent: hint || 'Метка'
		       }, {
		           iconLayout: 'default#image',
		           iconImageHref: 'img/m_pin.png',
		           iconImageSize: [27, 34],
		           iconImageOffset: [-13, -34]
		});
	},
	Cluster: function(){
		return new ymaps.Clusterer({
            clusterIcons: [{
                href: 'img/m_cluster.png',
                size: [48, 48],
                offset: [-24, -24]
            }],
            clusterNumbers: [100]
        });
	}
};
$(document).on('ready', ok26.App);
//# sourceMappingURL=main.js.map
